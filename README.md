# Corso di formazione su Intelligenza Artificiale

Questo corso è formato da 5 moduli.

* **MODULO 1**: fondamenti e text classification
* **MODULO 2**: named entity recognition
* **MODULO 3**: topic modeling
* **MODULO 4**: riepilogo 
* **MODULO 5**: generative ai e prompt engineering

