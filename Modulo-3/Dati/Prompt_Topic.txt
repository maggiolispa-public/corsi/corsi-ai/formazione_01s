Di seguito è riportato un elenco di topic estratti tramite un modello di Topic Modeling [LSA/pLSA/LDA/BERTopic] con corpora [BoW/TF-IDF].
I topic sono elencati in ordine di importanza decrescente.
Ogni topic è seguito dall'elenco delle parole chiave associate al topic ed ogni parola chiave ha un indice di rilevanza di quella parola all'interno del topic (nel formato: peso*"parola").
Date queste informazioni, dai un nome significativo ad ogni topic e riporta il nuovo elenco in output usando il formato Markdown per migliorare la leggibilità.
I nomi dei topic devono essere in italiano. Il resto del testo deve essere mantenuto nella lingua originale.

<ELENCO_TOPIC>
....
....
</ELENCO_TOPIC>